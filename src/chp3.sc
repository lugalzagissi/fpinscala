object chp3 {
  sealed trait List[+A]
  case object Nil extends List[Nothing]
  case class Cons[+A](head: A, tail: List[A]) extends List[A]
  object List {
    def sum(ints: List[Int]): Int = ints match {
      case Nil => 0
      case Cons(x, xs) => x + sum(xs)
    }
    def product(ds: List[Double]): Double = ds match {
      case Nil => 1.0
      case Cons(0.0, _) => 0.0
      case Cons(x, xs) => x * product(xs)
    }
    def apply[A](as: A*): List[A] =
      if (as.isEmpty) Nil
      else Cons(as.head, apply(as.tail: _*))
  }
  // exercise 3.1
  val x = List(1, 2, 3, 4, 5) match {
    case Cons(x, Cons(2, Cons(4, _))) => x
    case Nil => 42
    case Cons(x, Cons(y, Cons(3, Cons(4, _)))) => x + y
    case Cons(h, t) => h + List.sum(t)
    case _ => 101
  }
  // exercise 3.2
  val y = List(1, 2, 3, 4, 5)
  def tail[A](l: List[A]): List[A] = l match {
    case Nil => l
    case Cons(x, xs) => xs
  }

  tail(y)
  tail(Nil)
  // exercise 3.3
  def setHead(l: List[Int], i: Int): List[Int] = l match {
    case Nil => l
    case Cons(x, xs) => Cons(i, xs)
  }

  setHead(y, 100)
  setHead(y, 6)
  // exercise 3.4
  def drop(l: List[Int], n: Int): List[Int] = l match {
    case Nil => l
    case Cons(x, xs) =>
      def helper(m: List[Int], acc: Int): List[Int] = {
        if (acc == 0) m
        else helper(tail(m), acc - 1)
      }

      helper(l, n)
  }

  drop(y, 10)
  val z = List(1, 2)
  // exercise 3.5
  def dropWhile[A](l: List[A])( f: A => Boolean): List[A] = l match {
    case Nil => Nil
    case Cons(x, xs) => {
      if (f(x)) dropWhile(xs)( f)
      else Cons(x, dropWhile(xs)( f))
    }
  }

  dropWhile[Int](y) (x => x > 3)
  // exercise 3.6
  def init[A](l: List[A]): List[A] = l match {
    case Nil => Nil
    case Cons(x, Nil) => Nil
    case Cons(x, xs) => Cons(x, init(xs))
    }

  init(y)
  def foldRight[A, B](as: List[A], z: B)(f: (A, B) => B): B = as match {
    case Nil => z
    case Cons(x, xs) => f(x, foldRight(xs, z)(f))
  }

  def sum2(ns: List[Int]) = foldRight(ns, 0)((x, y) => x + y)
  def product2(ns: List[Int]) = foldRight(ns, 1.0)(_ * _)
  sum2(y)
  product2(y)
  // exercise 3.8
  foldRight(List(1,2,3), Nil: List[Int])(Cons(_,_))
  //exercise 3.9
  def length[A](as: List[A]): Int = foldRight(as, 0)((x, y) => y + 1)
  length(y)
  // exercise 3.10
  def foldLeft[A, B](as: List[A], z: B)(f: (B, A) => B): B = as match {
    case Nil => z
    case Cons(x, xs) => foldLeft(xs, f(z, x))(f)
  }

  foldLeft(y, 0)((x,y) => x + 1)
  // exercise 3.11
  def sum3(ns: List[Int]): Int = foldLeft(ns, 0)((x,y) => x + y)
  sum3(y)
  def product3(ns: List[Int]): Int = foldLeft(ns, 1)((x,y) => x * y)
  product3(y)
  def length2[A](ns: List[A]): Int = foldLeft(ns, 0)((x,y) => x + 1)
  length2(y)
  // exercise 3.12
  def reverse[A](ns: List[A]): List[A] = foldLeft(ns, List[A]())((a,b) => Cons(b,a))
  reverse(y)
  reverse(Nil)
  // exercise 3.13
  def append[A](ns: List[A], ms: List[A]): List[A] = foldLeft(ns, ms)((b,a) => Cons(a, b))
  val w = List(6,7,8)
  append(y, Nil)
  append(y, z)
  append(z, w)
  // exercise 3.14
  def concatenate[A](ns: List[List[A]]): List[A] = foldLeft(ns, List[A]())((b,a) => append(a,b))
  concatenate(List(y,w))
  // exercise 3.16
  def plusOne(ns: List[Int]): List[Int] = ns match {
    case Nil => Nil
    case Cons(x, xs) => Cons(x + 1, plusOne(xs))
  }

  plusOne(y)
  // exercise 3.17
  def doubleToString(ns: List[Double]): List[String] = ns match {
    case Nil => Nil
    case Cons(x, xs) => Cons(x.toString, doubleToString(xs))
  }

  doubleToString(List(1.0,2.0))
  // exercise 3.18
  def map[A, B](as: List[A])(f: A => B): List[B] = as match {
    case Nil => Nil
    case Cons(x, xs) => Cons(f(x), map(xs)(f))
  }

  map(y)(x => x + 1)
  // exercise 3.19
  def filter[A](as: List[A])(f: A => Boolean): List[A] = as match {
    case Nil => Nil
    case Cons(x, xs) => if (f(x)) Cons(x, filter(xs)(f)) else filter(xs)(f)
  }

  filter(y)(x => x < 3)
  // exercise 3.20
  def flatMap[A,B](as: List[A])(f: A => List[B]): List[B] = as match {
    case Nil => Nil
    case Cons(x, xs) => concatenate(List(f(x), flatMap(xs)(f)))
  }

  flatMap(w)(x => List(x,x))
  // exercise 3.22
  def addLists(as: List[Int], bs: List[Int]): List[Int] = as match {
    case Nil => bs
    case Cons(x, xs) => bs match {
      case Nil => as
      case Cons(y, ys) => Cons(x + y, addLists(xs, ys))
    }
  }

  addLists(w, w)
  addLists(y, w)
  // exercise 3.23
  def zipWith[A](as: List[A], bs: List[A])(f: (A, A) => A): List[A] = as match {
    case Nil => bs
    case Cons(x, xs) => bs match {
      case Nil => as
      case Cons(y, ys) => Cons(f(x, y), zipWith(xs, ys)(f))
    }
  }

  zipWith(w, w)((x,y) => x + y)
  zipWith(w, w)((x,y) => x*y)
  // exercise 3.24
  def hasSubsequence[A](sup: List[A], sub: List[A]): Boolean = {
    def head(l: List[A]): Option[A] = l match {
      case Nil => None
      case Cons(x, xs) => Some(x)
    }

    sup match {
      case Nil => sub match {
        case Nil => true
        case Cons(y, ys) => false
      }
      case Cons(x, xs) => sub match {
        case Nil => true
        case Cons(z, zs) => if ((x == z) && (head(xs) == head(zs))) hasSubsequence(xs, zs)
                            else if ((x == z) && (tail(sub) == Nil)) true
                            else hasSubsequence(xs, sub)
      }
    }
  }


  hasSubsequence(y, List(2,3,4))
  hasSubsequence(y, List(1,2,3))
  hasSubsequence(y, List(1))
  hasSubsequence(y, List(6))
  hasSubsequence(y, List(3,2,1))
  hasSubsequence(y, Nil)
  hasSubsequence(Nil, List(1))
  hasSubsequence(y, List(4,5))
  sealed trait Tree[+A]
  case class Leaf[A](value: A) extends Tree[A]
  case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]
  val oak = Branch(Leaf(1), Branch(Leaf(2), Leaf(5)))
  val maple = Branch(Leaf(5), Branch(Leaf(6), Branch(Leaf(7), Branch(Leaf(8), Branch(Leaf(9), Leaf(10))))))

  // exercise 3.25
  def size[A](tree: Tree[A]): Int = {
    def helper(bush: Tree[A], acc: Int): Int = {
      bush match {
        case Leaf(i) => acc + 1
        case Branch(branch1, branch2) => helper(branch1, acc) + helper(branch2, acc)
      }
    }

    helper(tree, 0)
  }
  size(oak)
  // exercise 3.26
  def maximum(tree: Tree[Int]): Int = {
    def helper(bush: Tree[Int], acc: Int): Int = bush match {
      case Leaf(i) => i.max(acc)
      case Branch(branch1, branch2) => maximum(branch1) max maximum(branch2)
    }
    helper(tree, 0)
  }
  maximum(oak)
  // exercise 3.27
  def depth[A](tree: Tree[A]): Int = {
    def helper(bush: Tree[A], acc: Int): Int = bush match {
      case Branch(branch1, branch2) => (helper(branch1, acc) + 1) max (helper(branch2, acc) + 1)
      case Leaf(i) => acc + 1
    }
    helper(tree, 0)
  }

  depth(oak)
  depth(maple)
  // exercise 3.28
  def map[A](tree: Tree[A])(f: A => A): Tree[A] = tree match {
    case Leaf(i) => Leaf(f(i))
    case Branch(branch1, branch2) => Branch(map(branch1)(f), map(branch2)(f))
  }
  map(oak)(x => x + 10)
  // exercise 3.29
  def fold[A, B](tree: Tree[A])(l: A => B)(b: (B,B) => B): B = tree match {
    case Leaf(i) => l(i)
    case Branch(branch1, branch2) => b(fold(branch1)(l)(b), fold(branch2)(l)(b))
  }

  size(maple)
  fold(maple)(x => 1)((a,b) => a + b)

  maximum(maple)
  fold(maple)(x => x)((a,b) => a max b)

  depth(oak)
  fold(oak)(x => 1)((a,b) => a max b)

  map(oak)(x => x + 10)
  
}