import java.util.concurrent.{Callable, Executors, ExecutorService,Future}

import scala.collection.immutable.List

import scala.concurrent.duration.TimeUnit

type Par[A] = ExecutorService => Future[A]

def unit[A](a: A): Par[A]
def fork[A](a: => Par[A]): Par[A]
def lazyUnit[A](a: => A): Par[A] = fork(unit(a))
def run[A](s: ExecutorService)(a: Par[A]): Future[A] = a(s)
/**
abstract class ExecutorService {
  def submit[A](a: Callable[A]): Future[A]
}

trait Callable[A] {def call: A}

trait Future[A]{
  def get: A
  def get(timeout: Long, unit: TimeUnit): A
  def cancel(evenIfRunning: Boolean): Boolean
  def isDone: Boolean
  def isCancelled: Boolean
}
*/
object Par {
  def unit[A](a: A): Par[A] = (es: ExecutorService) => UnitFuture(a)

  private case class UnitFuture[A](get: A) extends Future[A] {
    def isDone = true
    def get(timeout: Long, units: TimeUnit) = get
    def isCancelled = false
    def cancel(evenIfRunning: Boolean): Boolean = false
  }

  /**
    * function that combines the results of two parallel computations using an arbitrary binary function
    */
  def map2[A,B,C](a: Par[A], b: Par[B])(f: (A,B) => C): Par[C] =
    (es: ExecutorService) => {
      val af = a(es)
      val bf = b(es)
      UnitFuture(f(af.get, bf.get))
    }

  def fork[A](a: => Par[A]): Par[A] =
    es => es.submit(new Callable[A] {
      def call = a(es).get
    })

  def lazyUnit[A](a: => A): Par[A] = fork(unit(a))

  def map[A,B](pa: Par[A])(f: A => B): Par[B] = map2(pa, unit(()))((a,_) => f(a))

  // exercise 7.4
  def asyncF[A,B](f: A => B): A => Par[B] = {x => lazyUnit(f(x))}

  def sortPar(parList: Par[List[Int]]): Par[List[Int]] = map(parList)(_.sorted)

  // exercise 7.5
  /**
    * function that collects the results of a list of parallel computations into a list
    */
  def sequence[A](ps: List[Par[A]]): Par[List[A]] = ps match {
    case Nil => unit(Nil)
    case h :: t => map2(h, sequence(t))((x,y) => x :: y)
  }

  /**
    * function that maps over a list in parallel using an arbitrary function
    */
  def parMap[A,B](ps: List[A])(f: A => B): Par[List[B]] = fork {
    val fbs: List[Par[B]] = ps.map(asyncF(f))
    sequence(fbs)
  }

  val myList = List(1,2,3,4,5)

  // exercise 7.6
  def parFilter[A](as: List[A])(f: A => Boolean): Par[List[A]] = {
    parMap(as.map(asyncF(a => if (f(a)) List(a) else Nil)).flatten)(x => x)
  }

  def equal[A](e: java.util.concurrent.ExecutorService)(p: Par[A], p2: Par[A]): Boolean =
    p(e).get == p2(e).get




}