object chp5 {

  def maybeTwice(b: Boolean, i: => Int) = {
    lazy val j = i
    if (b) j + j else 0}
  val x = maybeTwice(true, {println("hi \t"); 1 + 41})
  sealed trait Stream[+A] {
    // exercise 5.1
    def toList: List[A] = this match {
      case Cons(x, xs) => x() :: xs().toList
      case Empty => List()
    }

    def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
      lazy val head = hd
      lazy val tail = tl
      Cons(() => head, () => tail)
    }

    def empty[A]: Stream[A] = Empty

    // exercise 5.2-1
    def take(n: Int): Stream[A] = {
      def helper(m: Int, l: Stream[A], s: Stream[A]): Stream[A] = s match {
        case Empty => l
        case Cons(x, xs) => if (m == 0) l else helper(m - 1, Cons(x, () => l), xs())
      }
      helper(n, Empty, this)
    }
    // exercise 5.2-2
    def drop(n: Int): Stream[A] = this match {
      case Empty => Empty
      case Cons(x, xs) => if (n == 0) this else xs().drop(n - 1)
    }

    def exists(p: A => Boolean): Boolean = this match {
      case Cons(h, t) => p(h()) || t().exists(p)
      case _ => false
    }
    // exercise 5.3
    def takeWhile(p: A => Boolean): Stream[A] = this match {
      case Cons(x, xs) => if (p(x())) Cons(x, () => xs().takeWhile(p)) else xs().takeWhile(p)
      case _ => Empty
    }

    def foldRight[B](z: => B)(f: (A, => B) => B): B = this match {
      case Cons(h, t) => f(h(), t().foldRight(z)(f))
      case _ => z
    }

    // exercise 5.4
    def forAll(p: A => Boolean): Boolean = this.foldRight(true)((a, b) => p(a) && b)

    // exercise 5.5
    def takeWhile2(p: A => Boolean): Stream[A] =
      this.foldRight(empty[A])((a,b) => if (p(a)) cons(a, b) else empty)

    // exercise 5.7
    def map[B](f: A => B): Stream[B] = this.foldRight(empty[B])((a,b) => cons(f(a),b))

    def filter(f: A => Boolean): Stream[A] = this.foldRight(empty[A])((a,b) =>
      if(f(a)) Cons(() => a, () => b) else b)

    def append[B >: A](s: => Stream[B]): Stream[B] = this.foldRight(s)((a,b) => cons(a,b))

    def flatMap[B](f: A => Stream[B]): Stream[B] = this.foldRight(empty[B])((a,b) => f(a).append(b))

    // exercise 5.13
    def mapViaUnfold[B](f: A => B): Stream[B] = unfold(this){case Cons(h,t) => Some(f(h()), t()); case _ => None}


  }
  val y = Cons(() => 1,() => Cons(() => 2, () => Empty))
  val z = y.drop(1).toList
  val a = Stream(5,6,7).take(1).toList
  val b = Stream( 5,6,7,8).takeWhile(_ > 6).toList
  val c = Stream(1,2,3,4,5).forAll(_ > 0).toString
  val d = Stream(0,1,2).forAll(_ > 0).toString
  val e = Stream(0,1,2,3).map(x => x*x).toList
  val ones: Stream[Int] = Stream.cons(1, ones)
  ones.take(5).toList
  ones.map(_ + 1).exists(_ % 2 == 0)
  ones.takeWhile(_ == 1)
  ones.forAll(_ != 1)

  // exercise 5.8
  def constant[A](a: A): Stream[A] = Stream.cons(a, constant(a))

  val fives = constant(5)
  fives.take(5).toList
  // exercise 5.9
  def from(n: Int): Stream[Int] = Stream.cons(n, from(n + 1))
  
  val fromTwo = from(2)
  fromTwo.take(5).toList
  // exercise 5.10
  def fibs: Stream[Int] = {

    def fibonacci(n: Int): Int = {
      if (n == 0) 1
      else if (n == -1) 0
      else fibonacci(n - 1) + fibonacci(n - 2)
    }

    def fibs2(m: Int): Stream[Int] = Stream.cons(fibonacci(m), fibs2(m + 1))
    fibs2(0)
  }

  val fibs2 = {
    def go(f0: Int, f1: Int): Stream[Int] =
      Stream.cons(f0, go(f1, f0+f1))
    go(0, 1)
  }

  val fn = fibs
  fn.take(10).toList
  // exercise 5.11
  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = f(z) match {
    case Some((h, s)) => Stream.cons(h, unfold(s)(f))
    case None => Stream.empty
  }

  // exercise 5.12
  def fibs3: Stream[Int] = unfold((0,1))(p => p match {case (f0,f1) => Some(f0,(f1, f0 + f1))})

  def from2(n: Int): Stream[Int] = unfold(n){case a => Some(a, a + 1)}

  def constant2[A](a: A): Stream[A] = unfold(a){case n => Some(a, a)}

  def ones2: Stream[Int] = unfold(1){case 1 => Some(1,1)}

  fibs3.take(10).toList
  from2(3).take(10).toList
  constant2("xxx").take(10).toList
  ones2.take(10).toList
  case object Empty extends Stream[Nothing]
  case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]
  object Stream {
    def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
      lazy val head = hd
      lazy val tail = tl
      Cons(() => head, () => tail)
    }
    def empty[A]: Stream[A] = Empty
    def apply[A](as: A*): Stream[A] =
      if (as.isEmpty) empty else cons(as.head, apply(as.tail: _*))
    /**
    def headOption: Option[A] = this match {
    case Empty => None
    case Cons(h, t) => Some(h())
  }
      */
  }
}