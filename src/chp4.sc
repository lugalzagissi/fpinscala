object chp4 {
  def failingFn(i: Int): Int = {
    //val y: Int = throw new Exception("fail!")
    try {
      val x = 42 + 5
      x + ((throw new Exception("fail!")): Int)
    }
    catch {
      case e: Exception => 43
    }
  }
  failingFn(5)
  // exercise 4.1
  sealed trait Option[+A] {
    def map[B](f: A => B): Option[B] = this match {
      case None => None
      case Some(a) => Some(f(a))
    }
    def flatMap[B](f: A => Option[B]): Option[B] = this.map(f).getOrElse(None)
    def getOrElse[B >: A](default: => B): B = this match {
      case None => default
      case Some(a) => a
    }
    def orElse[B >: A](ob: => Option[B]): Option[B] = this match {
      case None => ob
      case Some(a) => Some(a)
    }
    def filter(f: A => Boolean): Option[A] = this match {
      case None => None
      case Some(a) => if (f(a)) this else None
    }
  }




  case class Some[+A](get: A) extends Option[A]

  case object None extends Option[Nothing]

  def mean(xs: Seq[Double]): Option[Double] =
    if (xs.isEmpty) None
    else Some(xs.sum / xs.length)

  // exercise 4.2
  def variance(xs: Seq[Double]): Option[Double] = {
    mean(xs).flatMap(m => mean(xs.map(x => math.pow(x - m, 2))))
  }

  def lift[A, B](f: A => B): Option[A] => Option[B] = _ map f

  val x: Option[Int] => Option[Int] = lift(failingFn)

  /**
  def parseInsuranceQuote(age: String, numberOfSpeedingTickets: String): Option[Double] = {
    val optAge: Option[Int] = Try(age.toInt)
    val optTickets: Option[Int] = Try(numberOfSpeedingTickets.toInt)
    map2(optAge, optTickets)(insuranceRateQuote)
  }

  def Try[A](a: => A): Option[A] = try Some(a)
  catch (case e: Exception => None)
    */

  // exercise 4.3
  def map2[A, B, C](a: Option[A], b: Option[B])(f: (A, B) => C): Option[C] = {
    a.flatMap(x => b.map(y => f(x, y)))
  }

  map2(Some(3), Some(2))((x,y) => x * y)

  // exercise 4.4
  def sequence[A](a: List[Option[A]]): Option[List[A]] = {

    val accumulator: List[A] = Nil

    def helper(listOfOptions: List[Option[A]], acc: List[A]): List[A] = {
      listOfOptions match {
        case Nil => acc
        case head :: xs =>
          if (head == None) Nil
          else {
            val headValue = head.getOrElse(Nil).asInstanceOf[A]
            val newacc:List[A] = headValue :: acc
            helper(xs, newacc)
          }
      }
    }
    Some(helper(a, accumulator))
  }

  val seq = List(Some(1),Some(2),Some(3))

  sequence(seq)

  // exercise 4.5
  def traverse[A,B](a: List[A])(f: A => Option[B]): Option[List[B]] = {
    a match {
      case Nil => Some(Nil)
      case x :: xs => f(x).flatMap(y => traverse(xs)(f).map(z => y :: z))
    }
  }

  def sequenceNew[A](a: List[Option[A]]): Option[List[A]] = {
    traverse(a)(x => x)
  }

  traverse(seq)(x => Some(x))
  sequenceNew(seq)

  trait Either[+E, +A] {

    def map[B](f: A => B): Either[E, B] = this match {
      case Left(e) => Left(e)
      case Right(b) => Right(f(b))
    }

    def flatMap[EE >: E, B](f: A => Either[EE, B]): Either[EE, B] = this match {
      case Left(a) => Left(a)
      case Right(b) => f(b)
    }

    def orElse[EE >: E, B >: A](b: => Either[EE, B]): Either[EE, B] = this match {
      case Left(a) => b
      case Right(c) => Right(c)
    }

    def map2[EE >: E, B, C](b: Either[EE, B])(f: (A, B) => C): Either[EE, C] =
      this match {
        case Left(a) => Left(a)
        case Right(c) => this.flatMap(x => b.map(y => f(x,y)))
      }
  }

  case class Left[+E](value: E) extends Either[E, Nothing]
  case class Right[+A](value: A) extends Either[Nothing, A]

  // exercise 4.7
  def sequenceEither[E, A](es: List[Either[E, A]]): Either[E, List[A]] =
    traverseEither(es)(x => x)

  def traverseEither[E, A, B](as: List[A])(f: A => Either[E, B]): Either[E, List[B]] = as match {
    case Nil => Right(Nil)
    case x :: xs => f(x).flatMap(y => traverseEither(xs)(f).map(z => y :: z))
  }
}