import scala.util.Random

trait RNG {
  def nextInt: (Int, RNG)
}

case class SimpleRNG(seed: Long) extends RNG {
  def nextInt: (Int, RNG) = {
    val newSeed = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL
    val nextRNG = SimpleRNG(newSeed)
    val n = (newSeed >>> 16).toInt
    (n, nextRNG)
  }
}

Int.MinValue
Int.MaxValue

// exercise 6.1
def nonNegativeInt(rng: RNG): (Int, RNG) = {
  val (number,newRng) = rng.nextInt
  if (number > 0) (number,newRng)
  else (-(number + 1), newRng)
}

def randomPair(rng: RNG): (Int, Int) = {
  val (i1,_) = rng.nextInt
  val (i2,_) = rng.nextInt
  (i1,i2)
}

val myRng = SimpleRNG(1)
randomPair(myRng)



// exercise 6.2
def double(rng: RNG): (Double, RNG) = {
  val max = Int.MaxValue.toDouble
  val nonNegInt = nonNegativeInt(rng)
  (nonNegInt._1/(max + 1),nonNegInt._2)
}

val (a,b) = double(myRng)
double(b)

// exercise 6.3
def intDouble(rng: RNG): ((Int, Double), RNG) = {
  val (i, r) = rng.nextInt
  val (d, r2) = double(r)
  ((i,d), r2)
}

val (c,d) = intDouble(myRng)
intDouble(d)

def doubleInt(rng: RNG): ((Double, Int), RNG) = {
  val (d, r1) = double(rng)
  val (i, r2) = r1.nextInt
  ((d, i), r2)
}

def double3(rng: RNG): ((Double, Double, Double), RNG) = {
  val (d1, r1) = double(rng)
  val (d2, r2) = double(r1)
  val (d3, r3) = double(r2)
  ((d1, d2, d3), r3)
}

// exercise 6.4
def ints(count: Int)(rng: RNG): (List[Int], RNG) = {
  val l = Nil
  def helper(c: Int)(r: RNG)(l: List[Int]): (List[Int], RNG) = {
    val (i, p) = r.nextInt
    if (c <= 0) (l, r) else helper(c - 1)(p)(i :: l)
  }
  helper(count)(rng)(l)
}

ints(10)(myRng)

type Rand[+A] = RNG => (A, RNG)

val int: Rand[Int] = _.nextInt

def unit[A](a: A): Rand[A] = rng => (a, rng)

def map[A, B](s: Rand[A])(f: A => B): Rand[B] = rng => {
  val (a, rng2) = s(rng)
  (f(a), rng2)
}

// exercise 6.5
def doubleViaMap(rng: RNG): (Double, RNG) = {
  map(nonNegativeInt)(x => x / (Int.MaxValue.toDouble + 1))(rng)
}

doubleViaMap(myRng)

// exercise 6.6
def map2[A,B,C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] = rng => {
  val (x, r1) = ra(rng)
  val (y, r2) = rb(r1)
  (f(x,y), r2)
}

map2(double, double)((x,y) => (x,y))(myRng)

def both[A,B](ra: Rand[A], rb: Rand[B]): Rand[(A,B)] = map2(ra, rb)((_, _))

val randIntDouble: Rand[(Int, Double)] = both(int, double)

val randDoubleInt: Rand[(Double, Int)] = both(double, int)

// exercise 6.8
def flatMap[A,B](f: Rand[A])(g: A => Rand[B]): Rand[B] = rng => {
  val (a, r) = f(rng)
  g(a)(r)
}

flatMap(double)(unit)

def nonNegativeLessThan(n: Int): Rand[Int] = flatMap(nonNegativeInt){ i =>
  val mod = i % n
  if (i + (n - 1) - mod >= 0) unit(mod) else nonNegativeLessThan(n)
}